﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridBuilder : MonoBehaviour {

    public GameObject TileHolder;
    public GameObject Tile;

    public static int GridSize = 8;
   
    public void BuildGrid(Tiles tilesInstance) {
        TileHolder.GetComponent<GridLayoutGroup>().constraintCount = GridSize;
        int id = 0;

        for (int x = -(GridSize / 2); x < (GridSize / 2); x++) {
            for (int y = -(GridSize / 2); y < (GridSize / 2); y++) {
                GameObject instantiated = Instantiate(Tile, TileHolder.transform, true);                
                instantiated.GetComponent<ButtonTile>().tileID = id;
                Tile newTile = new Tile(id, instantiated);
                tilesInstance.AddTile(newTile, id);
                id++;
            }
        }
    }

    
}
