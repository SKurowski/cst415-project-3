﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player {

    Color playerColor;
    int score;
    int id;

    public Player(Color tileColor, int id) {
        this.playerColor = tileColor;
        this.id = id;
    }

    public int GetId() {
        return this.id;
    }

    public void PaintTile(Tile tile) {

        if (tile.TileObj.GetComponent<Image>().color != this.playerColor) {
            if (tile.TileObj.GetComponent<Image>().color != Color.white) {
                tile.TileObj.GetComponent<Image>().color = Color.white;
                tile.Player = new Player(Color.white, -1);
            } else {
                tile.TileObj.GetComponent<Image>().color = this.playerColor;
                tile.Player = this;
            }
        }
    }

    public Color GetColor() {
        return this.playerColor;
    }

    public int GetScore() {
        return this.score;
    }

    public int SetScore(int score) {
        return this.score = score;
    }

    public void PaintAdjacentTiles(Tile centerTile, Tiles tilesInstance) {
        int clickedTileId = centerTile.TileObj.GetComponent<ButtonTile>().tileID;
        int leftId = (clickedTileId % GridBuilder.GridSize == 0) ? -1 : (clickedTileId - 1);
        int rightId = (clickedTileId % GridBuilder.GridSize == (GridBuilder.GridSize - 1)) ? -1 : (clickedTileId + 1);
        int upId = (clickedTileId - GridBuilder.GridSize < 0) ? -1 : (clickedTileId - GridBuilder.GridSize);
        int downId = (clickedTileId + GridBuilder.GridSize > (Mathf.Pow(GridBuilder.GridSize, 2) - 1)) ? -1 : (clickedTileId + GridBuilder.GridSize);

        Tile leftTile = tilesInstance.GetTileAtID(leftId);
        if (leftTile != null && leftTile.TileObj.GetComponent<Image>().color != this.playerColor) {
            PaintTile(leftTile);
        }

        Tile rightTile = tilesInstance.GetTileAtID(rightId);
        if (rightTile != null && rightTile.TileObj.GetComponent<Image>().color != this.playerColor) {
            PaintTile(rightTile);
        }

        Tile upTile = tilesInstance.GetTileAtID(upId);
        if (upTile != null && upTile.TileObj.GetComponent<Image>().color != this.playerColor) {
            PaintTile(upTile);
        }

        Tile downTile = tilesInstance.GetTileAtID(downId);
        if (downTile != null && downTile.TileObj.GetComponent<Image>().color != this.playerColor) {
            PaintTile(downTile);
        }
    }
}
