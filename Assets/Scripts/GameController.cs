﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Tile {

    public GameObject TileObj;
    public int Id;
    public Player Player;
    public double Rank;
    public bool Open;

    public Tile(int id, GameObject tileObj) {
        this.Id = id;
        this.TileObj = tileObj;
        Rank = 0;
        Player = new Player(Color.white, -1);
        Open = true;
    }
}

public class GameController : MonoBehaviour {

    //Instance of this class
    private static GameController game;

    //Editor Variables
    public GridBuilder builder;
    public Color playerColor, aiColor;
    public GameObject playerText, aiText;
    public int numTurns;
       
    //Class Instances
    Tiles tiles;
    Player human, ai;

    public static GameController instance {
        get {
            if (!game) {
                game = FindObjectOfType(typeof(GameController)) as GameController;

                if (!game) {
                    Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                }
            }

            return game;
        }
    }

    private void Start() {

        tiles = new Tiles();
        builder.BuildGrid(tiles);

        human = new Player(playerColor, 0);
        ai = new Player(aiColor, 1);

        UpdateVisibleScore();
    }

    public void ButtonPressed(int id) {
        //Debug.Log("Button: " + id + " pressed");
        if (numTurns > 0) {
            Tile tileClicked = tiles.GetTileAtID(id);
            human.PaintTile(tileClicked);
            human.PaintAdjacentTiles(tileClicked, this.tiles);
            //tileClicked.Open = false;

            Tile aiMove = GetBestMove(tiles, ai);
            Debug.Log(aiMove.Id);
            ai.PaintTile(aiMove);
            ai.PaintAdjacentTiles(aiMove, this.tiles);
            

            human.SetScore(tiles.CalculateScores(human, ai)[0]);
            ai.SetScore(tiles.CalculateScores(human, ai)[1]);
            UpdateVisibleScore();
            numTurns--;
            //tileClicked.Open = true;
        }
    }
        
    void RandomAiMove() {
        int idToPaint = Random.Range(0, tiles.NumTiles());

        Tile tile = tiles.GetTileAtID(idToPaint);
        ai.PaintTile(tile);
        ai.PaintAdjacentTiles(tile, this.tiles);
    }

    Tile GetBestMove(Tiles t, Player p) {

        Tile bestTile = null;
        List<Tile> openTiles = t.OpenTiles(p);
        Tiles newTiles;

        Player otherPlayer = (p.GetId() == 0) ? ai : human;
    
        for (int i = 0; i < openTiles.Count; i++) {
            newTiles = t.Clone();
            Tile newTile = openTiles[i];
            //newTile.Open = false;

            newTiles.SetTilePlayer(newTile.Id, p);
            newTiles.SetAdjacentTilePlayer(newTile.Id, p);

            int newScore = newTiles.CalculateScores(human, ai)[p.GetId()];

            if (newScore < p.GetScore() && numTurns > 0) {                
                Tile tempMove = GetBestMove(newTiles, otherPlayer);
                newTile.Rank = tempMove.Rank;
                //newTile.Open = true;
            } else {
                if (newScore == otherPlayer.GetScore()) {
                    newTile.Rank = 0;
                } else if (newScore < otherPlayer.GetScore()) {
                    newTile.Rank = newScore - otherPlayer.GetScore();
                } else if (newScore > otherPlayer.GetScore()) {
                    newTile.Rank = newScore - otherPlayer.GetScore();
                }
            }

            if (bestTile == null ||
               (p.GetId() == 0 && newTile.Rank < bestTile.Rank) ||
               (p.GetId() == 1 && newTile.Rank > bestTile.Rank)) {
                bestTile = newTile;
            }
        }

        
        return bestTile;
    }

    void UpdateVisibleScore() {
        playerText.GetComponent<Text>().text = "Player Score: " + human.GetScore();
        aiText.GetComponent<Text>().text = "AI Score: " + ai.GetScore();
    }

}
