﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonTile : MonoBehaviour, IPointerClickHandler {

    public int tileID;

    public void OnPointerClick(PointerEventData eventData) {
        //passes data to your button controller (or whater you need to know about the
        //button that was pressed), informing that this game object was pressed.
        GameController.instance.ButtonPressed(eventData.selectedObject.GetComponent<ButtonTile>().tileID);
    }

}
